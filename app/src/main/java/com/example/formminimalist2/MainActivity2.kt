package com.example.formminimalist2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val intent = intent
        if (intent != null) {
            val bundle = intent.getBundleExtra("bundle")
            val textViewValue = bundle?.getString("soal1")
            val radioButtonText = bundle?.getString("soal2")
            val spinnerValue = bundle?.getString("soal3")

            val prodi = findViewById<TextView>(R.id.kj1)
            val ospek = findViewById<TextView>(R.id.kj2)
            val filkom = findViewById<TextView>(R.id.kj3)
            prodi.text = textViewValue
            ospek.text = radioButtonText
            filkom.text = spinnerValue
        }

    }
}