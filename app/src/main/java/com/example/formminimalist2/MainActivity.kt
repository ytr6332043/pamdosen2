package com.example.formminimalist2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    var courses = arrayOf<String?>("Fakultas Ilmu Komputer", "Fakultas Ilmu Komunikasi", "Fakultas Ilmu Komedi")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val spin = findViewById<Spinner>(R.id.spinner)
        spin.onItemSelectedListener = this

        val ad: ArrayAdapter<*> = ArrayAdapter<Any?>(
            this,
            android.R.layout.simple_spinner_item,
            courses)

        ad.setDropDownViewResource(
            android.R.layout.simple_spinner_dropdown_item)
        spin.adapter = ad

        val submit = findViewById<Button>(R.id.button)
        submit.setOnClickListener {
            goToSecondActivity()
        }

    }

    override fun onItemSelected (parent: AdapterView<*>?,
                                view: View, position: Int,
                                id: Long) {
        Toast.makeText(applicationContext,
            courses[position],
            Toast.LENGTH_LONG)
            .show()
    }

    override fun onNothingSelected (parent: AdapterView<*>?) {}

    private fun goToSecondActivity() {
        val intent = Intent(this, MainActivity2::class.java)

        val soal1 = findViewById<TextView>(R.id.j1).text.toString()

        val soal2 = findViewById<RadioGroup>(R.id.j2).checkedRadioButtonId
        val radioButton: RadioButton = findViewById(soal2)
        val radioButtonText = radioButton.text.toString()

        val soal3 = findViewById<Spinner>(R.id.spinner).selectedItem.toString()

        val bundle = Bundle().apply{
            putString("soal1", soal1)
            putString("soal2",radioButtonText)
            putString("soal3", soal3)
        }

        intent.putExtra("bundle", bundle)
        startActivity(intent)

    }

}